class Challenge < ActiveRecord::Base
	#serialize :category_selected, Array

	## validations 
	validates :user_id,:session_type,:score,:time_take,:presence => true

	##relations
	has_many :challenge_details
	belongs_to :sender, class_name: 'User', :foreign_key => "user_id"
	belongs_to :winner, class_name: 'User', :foreign_key => "challenge_won_id"

	## scopes
	scope :all_challenges, lambda { |reciver_id,sender_id|where(user_id: [sender_id,reciver_id]).joins(:challenge_details).where("challenge_details.reciver_id in (?) and challenge_details.status = ?",[sender_id,reciver_id],"accepted") }
	scope :won_challenge, lambda{|reciver_id,sender_id,winner_id| all_challenges(reciver_id,sender_id).where("challenge_won_id =?",winner_id).length}
	#scope :lost_challenge, lambda{|user_id|where("user_id =? or reciver_id = ? or challenge_won_id =?",user_id,user_id,user_id).size}

	## callbacks
	#after_create :challenge_extra_details

	## instance methods
	def challenge_extra_details
		recivers = self.challenge_details
		sender = self.sender
		if challenge_details.present?
			challenge_details.each do |challenge_detail|
			  	if challenge_detail.reciver_id.present?
				  	challenge_detail.reciver.send_challenge_friend("#{sender.firstname} has Challenge you via IQ-trainer") 
			  	else
			  		reciver = User.new(email: challenge_detail.reciver_email,password: "iqtrainer123",password_confirmation: "iqtrainer123", firstname: challenge_detail.reciver_email.split("@").first)
			  		reciver.save(:validate => false)
			  		challenge_detail.reciver_id = reciver.id
			  		challenge_detail.save(:validate => false)
			  		ChallengeFriend.mail_for_challenge(challenge_detail.reciver_email,sender).deliver
			  	end
		  	end
		end
	end

	def display_errors
    	self.errors.full_messages.join(', ')
	end

	def reciver_email
		reciver_email = ""
		challenge_details = self.challenge_details
		if challenge_details.length == 1
			reciver_email = challenge_details.last.reciver_email
		else
			if challenge_details.present?
				challenge_details.each do |challenge_detail|
					if challenge_detail.reciver_email.present?
						reciver_email = reciver_email + challenge_detail.reciver_email + ","
					end
				end
			end
		end
		return reciver_email
	end

	def reciver_name
		reciver_name = ""
		challenge_details = self.challenge_details
		if challenge_details.length == 1
			reciver_name = challenge_details.last.reciver_name
		else
			if challenge_details.present?
				challenge_details.each do |challenge_detail|
					if challenge_detail.reciver_name.present?
						reciver_name = reciver_name + challenge_detail.reciver_name + ","
					end
				end
			end
		end
		return reciver_name
	end

	
end

class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :city
      t.string :state
      t.string :country
      t.integer :phonenumber

      t.timestamps
    end
  end
end

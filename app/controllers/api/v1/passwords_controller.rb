class Api::V1::PasswordsController < Api::V1::ApplicationController
	before_filter :authentication_user_with_authentication_token, :only => [:change_password]
	def create
		if params[:email].present?
      @user = User.find_by_email(params[:email])
      if @user.present?
        @user.send_reset_password_instructions
        render_json({:message => "You will receive an email with instructions about how to reset your password in a few minutes.", :status => 200}.to_json)
      else
        render_json({:errors => "No User found with email #{params[:email]}", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Email Address is required", :status => 404}.to_json)
    end
	end
	
	def change_password
    if params[:user][:current_password].present? && params[:user][:password].present?
      @user = @current_user.update_with_password(changes_password_params)
      if @user
        render_json({:result=>{:messages =>"ok",:rstatus=>1, :errorcode =>""},:data=>{:messages =>"Your Password Successfully updated"}}.to_json)
      else
        render_json({:result=>{:messages =>@current_user.display_errors,:rstatus=>0, :errorcode => 404}}.to_json)
      end
    else
      render_json({:result=>{:messages =>"Current Password and Password required",:rstatus=>0, :errorcode => 404}}.to_json)
    end
  end
  
  private
  def changes_password_params
   params.require(:user).permit(:current_password, :password,:password_confirmation)
  end
end
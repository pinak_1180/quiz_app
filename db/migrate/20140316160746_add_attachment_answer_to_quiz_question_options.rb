class AddAttachmentAnswerToQuizQuestionOptions < ActiveRecord::Migration
  def self.up
    change_table :quiz_question_options do |t|
      t.attachment :answer
    end
  end

  def self.down
    drop_attached_file :quiz_question_options, :answer
  end
end

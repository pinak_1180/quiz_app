namespace :quiz_app do
  desc "This task is used to create questions with category and answers"
  task :create_question => :environment do
  	QuizCategory.delete_all
  	QuizQuestion.delete_all
  	QuizQuestionOption.delete_all
  	s = Roo::Excelx.new("IQ_trainer_sample.xlsx")
  	puts("+++++++++++++STARTS++++++++++++")
  	s.sheets.each do |sheet|
      s.default_sheet = sheet
      if s.default_sheet != "Visual logic"
      	2.upto(20) do |line|
          if s.first_row
        		if s.cell(line,'A').present?
              puts("+++++++++++++++++present")
        			quiz_category = QuizCategory.find_or_create_by_name(s.default_sheet)
              question      = quiz_category.quiz_questions.build
              question.quiz_category  = quiz_category
              question.question       = s.cell(line,'A')
              question.correct_answer = s.cell(line,'F')
              question.description    = s.cell(line,'G')
              question.quiz_question_options.build(name: s.cell(line,'B'))
              question.quiz_question_options.build(name: s.cell(line,'C'))
              question.quiz_question_options.build(name: s.cell(line,'D'))
              question.quiz_question_options.build(name: s.cell(line,'E'))
              unless quiz_category.save
                puts "errors"
              else
                puts("++++++++++++++++created+++++++++++++")
              end
        		end
          end
        end
      end
  	end
  end
end
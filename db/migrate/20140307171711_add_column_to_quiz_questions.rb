class AddColumnToQuizQuestions < ActiveRecord::Migration
  def change
    add_column :quiz_questions, :correct_answer, :string
    add_column :quiz_questions, :description, :string
  end
end

class DashboardController < ApplicationController
  before_action :authenticate_admin!
  def index
  	@users = User.all
 	@quiz_question = QuizQuestion.all
  end
end

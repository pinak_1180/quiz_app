class Api::V1::QuizQuestionsController < Api::V1::ApplicationController
	before_filter :authentication_user_with_authentication_token

  #this fetch random questions category wise
  def fetch_question
  	@question  = QuizQuestion.random.is_free.includes(:quiz_question_options,:quiz_category)
    @questions = @question.group_by &:quiz_category_id
  end
  
  def fetch_question_paid_app
    @question  = QuizQuestion.random.is_paid.includes(:quiz_question_options,:quiz_category)
    @questions = @question.group_by &:quiz_category_id    
  end

  #select questions based on category
  def question_by_category
  	if params[:id].present?
  		@questions = QuizQuestion.where(quiz_category_id: params[:id]).includes(:quiz_question_options,:quiz_category)
  	else
      render_json({:errors => "Id is required", :status => 404}.to_json)
    end
  end

  #select questions based on multiple categories

  def questions_by_multiple_category
    if params[:ids].present? && params[:is_free].present? && params[:num].present?
      @category_ids = params[:ids].split(/,\s*/)
      if @category_ids.length > params[:num].to_i
        @category_ids = @category_ids[0..(params[:num].to_i-1)]
      end
      if params[:is_free] == "1"
        @question  = QuizQuestion.random.by_category(@category_ids).is_free.includes(:quiz_question_options,:quiz_category)
        @question.update_all("attempted = attempted + 1")
        @questions = @question.group_by &:quiz_category_id
      else
        @question  = QuizQuestion.random.by_category(@category_ids).is_paid.includes(:quiz_question_options,:quiz_category)
        @questions = @question.group_by &:quiz_category_id
      end
      @category_numbers = Hash.new
      sum = 0
      total = @question.length > params[:num].to_i ? params[:num].to_i : @question.length
      category_length = @category_ids.length
      while sum <= total do
        diff = total - sum
        number = diff / category_length
        @category_ids.each do |category|
          counter = @category_numbers[category.to_i].present? ? @category_numbers[category.to_i]+number : number
          if @questions[category.to_i].length >= counter
            @category_numbers[category.to_i] = counter
          else
            @category_numbers[category.to_i] = @questions[category.to_i].length
            category_length = category_length - 1
          end
          sum = sum + @category_numbers[category.to_i]
        end
      end
    else
      render_json({:errors => "Required field missing", :status => 404}.to_json)
    end
  end
    
  def question_for_quick_session
   	@questions  = QuizQuestion.random.limit(50).includes(:quiz_question_options,:quiz_category)
   	#@question.update_all("attempted = attempted + 1")
   	#@questions = @question.group_by &:quiz_category_id
  end
    
  def global_avarage_for_questions
    if params[:questions_id].present? && params[:answers_id].present?
      @questions_ids = params[:questions_id].split(/,\s*/)
      @answers_ids = params[:answers_id].split(/,\s*/)
      @questions = QuizQuestion.by_id(@questions_ids).includes(:quiz_question_options,:quiz_category)
      @questions.each_with_index do |key,value|
        @quiz_question = QuizQuestion.find(key)
        if @quiz_question.correct_answer==@answers_ids[value]
          @quiz_question.update_attribute(:correct_answer_count,@quiz_question.correct_answer_count + 1)             
        end
      end
    else
      render_json({:errors => "Required field missing", :status => 404}.to_json)
    end   
  end
end
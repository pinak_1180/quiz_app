class QuizQuestionsController < ApplicationController
  before_action :set_quiz_question, only: [:show, :edit, :update, :destroy]
  add_breadcrumb 'Questions', :quiz_questions_path
  add_breadcrumb 'Create a new Question', '', :only => [:new, :create]
  add_breadcrumb 'Edit a Question', '', :only => [:edit, :update]
  add_breadcrumb 'Question', '', :only => [:show]

  # GET /quiz_questions
  def index
    @quiz_questions = QuizQuestion.page(params[:page]).per(10)
  end

  # GET /quiz_questions/1
  def show
  end

  # GET /quiz_questions/new
  def new
    @quiz_question = QuizQuestion.new
    @quiz_question.quiz_question_options.build
    @category_name = nil
    @category      = QuizCategory.where(is_image: true)
  end

  # GET /quiz_questions/1/edit
  def edit
    @category_name = @quiz_question.quiz_category.is_image
  end

  # POST /quiz_questions
  def create
    @quiz_question = QuizQuestion.new(quiz_question_params)
    if @quiz_question.save
      redirect_to @quiz_question, notice: 'Quiz question was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /quiz_questions/1
  def update
    if @quiz_question.update(quiz_question_params)
      redirect_to @quiz_question, notice: 'Quiz question was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /quiz_questions/1
  def destroy
    @quiz_question.destroy
    redirect_to quiz_questions_url, notice: 'Quiz question was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quiz_question
      @quiz_question = QuizQuestion.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def quiz_question_params
      params.require(:quiz_question).permit(:question,:quiz_category_id,:correct_answer,:description,:is_demo,:question_image,:answer_image,quiz_question_options_attributes:[:name,:id,:_destroy,:answer])
    end
end

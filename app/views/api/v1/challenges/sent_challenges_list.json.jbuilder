json.data do
	if @sent_challenges_list.empty?
		json.message "No Categories to list!"
	else		
		json.array! @sent_challenges_list do |c|
			json.id  c.challenge.id
			json.reciver_email  c.reciver_email
			json.reciver_name  c.reciver_name
			json.session_type     c.challenge.session_type
			json.score   c.challenge.score
			json.time_taken   c.challenge.time_take
			json.no_of_question c.challenge.no_of_question
			json.category_selected c.challenge.category_selected
			json.user_id @current_user.id
			json.sender_name @current_user.firstname
			json.status c.status
				
			json.created_at c.challenge.created_at
		end		
	end
end
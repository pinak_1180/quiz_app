class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.integer :user_id
      t.integer :reciver_id
      t.string :reciver_email
      t.string :session_type
      t.float :score
      t.string :time_take
      t.integer :no_of_question
      t.text :category_selected

      t.timestamps
    end
  end
end

class CreateQuizQuestionOptions < ActiveRecord::Migration
  def change
    create_table :quiz_question_options do |t|
      t.integer :quiz_question_id
      t.string :name

      t.timestamps
    end
  end
end

class Api::V1::SessionsController < Api::V1::ApplicationController
	before_filter :authentication_user_with_authentication_token, :only => [:destroy]

	def create
    @valid_email = params[:email].present?
    @valid_password = params[:password].present?
    @user = User.authenticate_user_with_auth(params[:email], params[:password])
    if @user.present?
       @user.check_duplicate_device_ids(params[:device_id],@user,params[:device_type]) 
      @authentication_token = @user.authentication_tokens.create(:auth_token => AuthenticationToken.generate_unique_token)
    elsif !@valid_password && !@valid_email
      render_json({:errors => "Email and password is required",:status => 404}.to_json)  
    else
      render_json({:errors => User.invalid_credential, :status => 404}.to_json)
    end
  end

  def destroy
    @token = AuthenticationToken.current_authentication_token_for_user(@current_user.id,params[:authentication_token]).first
    if @token.present?
      @token.destroy
      render_json({:message => "Logout Successfully!"}.to_json)
    else
      render_json({:errors => "No user found with authentication_token = #{params[:authentication_token]}"}.to_json)
    end
  end
end
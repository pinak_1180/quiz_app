class ChallengeDetail < ActiveRecord::Base

	## validations 
	validates_format_of :reciver_email, :with => /@/
	validates :reciver_email,:reciver_name, :presence => true

	##relations
	belongs_to :challenge
	belongs_to :reciver, class_name: 'User'

	##scopes
	scope :by_reciver,lambda { |reciver_id| where("reciver_id = ?", reciver_id) }
	scope :with_status,lambda { |status| where("status =?",status)}

	##callback
	before_create :set_default_status


	##instance methods
	def set_default_status
		self.status = "pending"
	end

	def status?(status)
		self.status == status.downcase
	end
end

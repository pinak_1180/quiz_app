class AddAttachmentAnswerImageToQuizQuestions < ActiveRecord::Migration
  def self.up
    change_table :quiz_questions do |t|
      t.attachment :answer_image
    end
  end

  def self.down
    drop_attached_file :quiz_questions, :answer_image
  end
end

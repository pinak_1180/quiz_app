json.data do
	json.id                @challenge.id
	json.reciver_email     @challenge.reciver_email
	json.reciver_name      @challenge.reciver_name
	json.sender_name       @challenge.sender.firstname
	json.sender_email      @challenge.sender.email
	json.session_type      @challenge.session_type
	json.score             @challenge.score
	json.time_taken        @challenge.time_take
	json.no_of_question    @challenge.no_of_question
	json.category_selected @challenge.category_selected
	json.user_id           @challenge.user_id
	json.created_at        @challenge.created_at
	json.challenge_won_id  @challenge.challenge_won_id
end
json.data do
	if @questions.empty?
		json.message "No Questions to view!"
	else		
		json.category @questions.each do |questions|
			@i = @category_numbers[questions.first]
			json.question questions.second.take(@i).each do |question|
				json.id  question.id
				question.attempted = question.attempted + 1
				question.save
				if question.quiz_category.is_image == true
					json.question question.question_url
					json.correct_answer question.answer_url
					json.answer_options question.answer_options do |option|
						json.answer_option option.option_url
					end

				else
					json.question question.question
					json.correct_answer question.correct_answer
					json.answer_options question.answer_options do |option|
						json.answer_option option.name
					end
				end
				json.description    question.description
				json.is_demo        question.is_demo
				json.quiz_category_id  question.quiz_category_id
				json.quiz_category_name question.quiz_category.name
			end
			@i = @i - 1
		end	
	end
end
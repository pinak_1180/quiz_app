class Api::V1::QuizCategoriesController < Api::V1::ApplicationController
	before_filter :authentication_user_with_authentication_token

	def list_categories
		@categories = QuizCategory.all
	end
end
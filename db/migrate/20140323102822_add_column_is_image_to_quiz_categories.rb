class AddColumnIsImageToQuizCategories < ActiveRecord::Migration
  def change
  	add_column :quiz_categories,:is_image,:boolean,default: false
  end
end

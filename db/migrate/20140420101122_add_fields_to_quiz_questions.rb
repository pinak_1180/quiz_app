class AddFieldsToQuizQuestions < ActiveRecord::Migration
  def change
  add_column :quiz_questions,:attempted,:integer,:default => 0
  add_column :quiz_questions,:correct_answer_count,:integer,:default => 0
  end
end

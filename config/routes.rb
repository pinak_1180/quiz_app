BuilderApp::Application.routes.draw do
  
  resources :quiz_categories do
    collection { post :import }
  end
  resources :quiz_questions
root 'dashboard#index'     
  get "api_help/index"
  resources :users
  devise_for :admins, path_names: {:sign_in => 'login', :sign_up => 'register', :sign_out => 'logout'},:controllers => {:sessions=>"admins/sessions",:passwords => "admins/passwords"}
  devise_for :users,path_names: {:sign_in => 'login'}

  #get "properties/document_category/:id" => "properties#document_category",:as =>:document_category
 


  #end
  namespace :api,:defaults => {:format => 'json'} do
    scope :module => :v1 do
      post 'signup' => 'registrations#create',:as=>:signup
      post 'login'  => 'sessions#create',:as => :end_user_login
      get  'logout' => 'sessions#destroy',     :as => :end_user_logout
      post 'forgot_password' => 'passwords#create'
      post 'change_password' => 'passwords#change_password'
      get  'fetch_question' => 'quiz_questions#fetch_question'
      get  'fetch_question_pro' => 'quiz_questions#fetch_question_paid_app'
      get  'question_by_category' => 'quiz_questions#question_by_category'
      get  'list_categories' => 'quiz_categories#list_categories'
      get  'questions_by_multiple_category' => 'quiz_questions#questions_by_multiple_category'
      get  'question_for_quick_session' => 'quiz_questions#question_for_quick_session'
      post 'global_avarage_for_questions' => 'quiz_questions#global_avarage_for_questions'
      post  'challenge/create'   => 'challenges#create'
      get   'challenge/sent_challenges_list' => 'challenges#sent_challenges_list'
      get   'challenge/recived_challenges_list' => 'challenges#recived_challenges_list'
      get   'challenge/show' => 'challenges#show'
      post  'challenge/won_lost' => 'challenges#won_lost'
      get  'challenge/challenge_statistics' => 'challenges#won_lost_statstics'
      post 'challenge/accept' => 'challenges#accept'
     end
  end

end
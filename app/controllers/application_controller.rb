class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_initial_breadcrumbs
  add_breadcrumb 'Dashboard','/'
  def after_sign_in_path(resources)
  end

  def set_initial_breadcrumbs
    #add_breadcrumb ("Home"),:root_path
  end
end

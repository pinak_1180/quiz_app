class QuizCategory < ActiveRecord::Base

	##Association##
	has_many :quiz_questions
  has_many :challenge_friends
  
  ##validations##
	validates :name,:presence => true

	#accepts_nested_attributes_for :quiz_questions, :allow_destroy => true
  

	def self.import(file)
    if file.content_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      QuizCategory.delete_all
      QuizQuestion.delete_all
      QuizQuestionOption.delete_all
      s = open_spreadsheet(file)
      puts("+++++++++++++STARTS++++++++++++")
      s.sheets.each do |sheet|
        s.default_sheet = sheet
        if s.default_sheet != "Visual logic" or s.default_sheet != "Sylogisms" or s.default_sheet != "Symbols" or s.default_sheet != "Spacial reasoning"
          2.upto(300) do |line|
            if s.first_row
              if s.cell(line,'A').present?
                puts("+++++++++++++++++present")
                quiz_category = QuizCategory.find_or_create_by_name(s.default_sheet)
                question                = quiz_category.quiz_questions.build
                question.quiz_category  = quiz_category
                question.question       = s.cell(line,'A')
                question.correct_answer = s.cell(line,'F')
=begin          if ans == 'a'
                  question.correct_answer = s.cell(line,'B')
                elsif ans == 'b'
                  question.correct_answer = s.cell(line,'C')
                elsif ans == 'c'
                  question.correct_answer = s.cell(line,'D')
                elsif ans == 'd'
                  question.correct_answer = s.cell(line,'E')
                else
                  question.correct_answer = "Not Available"                 
                end
=end
                question.description    = s.cell(line,'G')
                question.is_demo        = true if s.cell(line, 'H') == "free"
                question.quiz_question_options.build(name: s.cell(line,'B'))
                question.quiz_question_options.build(name: s.cell(line,'C'))
                question.quiz_question_options.build(name: s.cell(line,'D'))
                question.quiz_question_options.build(name: s.cell(line,'E'))
                unless quiz_category.save
                  puts "errors"
                else
                  puts("++++++++++++++++created+++++++++++++")
                end                
              end
            end
          end
        end
      end
      success = true
    else
    success = false
    end
    return success
  end


  def self.open_spreadsheet(file)
    begin
      case File.extname(file.original_filename)
      when ".xls" then Roo::Excel.new(file.path,nil,:ignore)
      when ".xlsx" then Roo::Excelx.new(file.path,nil,:ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    rescue RuntimeError => e
      puts e
    end
  end
end
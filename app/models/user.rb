class User < ActiveRecord::Base
include SendPush
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ##Associations## 
  has_many :authentication_tokens, :dependent => :destroy
  has_many :sent_challenges,:dependent => :destroy,:class_name => 'Challenge'
  has_many :challenge_details, through: :sent_challenges
  has_many :recived_challenges_details,:dependent => :destroy,:class_name => 'ChallengeDetail',:foreign_key => "reciver_id"
  
  ##Validations##
  #validates :city,:state,:phonenumber,:presence => true
  validates :firstname,:lastname,:presence => true,:format => {:with => /^[a-zA-z]+$/,:multiline => true,:message => "Only text is Allowed" }
  #validates_format_of :phonenumber, 
                      #:with => /\A[0-9]{10}\Z/,:length=>10

  ##scopes                    
  scope :get_user_device_ids,lambda { |device_id| where("device_id = ?", device_id) }
  scope :without_user, lambda{|user| where (user ? ["id != ?", user.id] : {}) }

  class << self
    def authenticate_user_with_auth(email, password)
      return nil unless email.present? or password.present?
      u = User.find_by_email(email)
      (u.present? && u.valid_password?(password))? u : nil
    end

    def invalid_credential
      "Email or Password is not valid"
    end
    def invalid_credentials
      "Email is required"
    end
  end

  def fullname
    "#{firstname} #{lastname}"
  end

  def has_android_device?
    device_type.downcase == "android"
  end

  def has_iphone_device?
    device_type.downcase == "iphone"
  end
  
  def check_duplicate_device_ids(device_id,user,device_type)
    @users=User.without_user(user).get_user_device_ids(device_id)
    if @users.present?
      @users.update_all({:device_id => nil})
    end
    user.device_id = device_id
    user.device_type = device_type
    user.save
  end
  
  def send_challenge_friend(msg)
		if self.device_id?
      if self.has_android_device?
       
        GCM.send_notification(self.device_id, {:message => msg}, :collapse_key => "Challenge", :time_to_live => 3600)
         puts "android"
      elsif self.has_iphone_device?	        
       
        APNS.send_notification(self.device_id, :alert => msg, :badge => 1, :sound => 'default')	              
        puts "iphone"
      end
    end
	end

  def display_errors
    self.errors.full_messages.join(', ')
  end

  def recived_challenges
    accepted = Challenge.where(id: self.recived_challenges_details.with_status("accepted").pluck("challenge_id").uniq).includes(:challenge_details,:sender)
    pending = Challenge.where(id: self.recived_challenges_details.with_status("pending").pluck("challenge_id").uniq).includes(:challenge_details,:sender)
    pending + accepted
  end
end
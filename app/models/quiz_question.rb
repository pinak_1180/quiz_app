class QuizQuestion < ActiveRecord::Base

	##Paperclip##
	has_attached_file :question_image,:url => "/system/question_images/:attachment/:id_partition/:style/:filename",:styles => {:thumb => "50x50>",:large => "200x200>" ,:small => "100x100>"}
	has_attached_file :answer_image,:url => "/system/answer_images/:attachment/:id_partition/:style/:filename",:styles => {:thumb => "50x50>",:large => "200x200>" ,:small => "100x100>"}
	
	##Association##
	belongs_to :quiz_category
	has_many   :quiz_question_options

	##Validations##
	#validates :question_or_question_image,:correct_answer_or_answer_image,:presence => true
	validates_attachment_content_type :question_image, :content_type => /\Aimage\/.*\Z/
	validates_attachment_content_type :answer_image, :content_type => /\Aimage\/.*\Z/

	
	accepts_nested_attributes_for :quiz_question_options, :allow_destroy => true

	## scopes##
	scope  :random, lambda {{:order=>'RAND()' } }
	scope  :is_free,->{where(is_demo: true)}
	scope  :is_paid,->{where(is_demo: false)}
	#scope  :random, lambda {{:order=>'RANDOM()' } }
	scope  :by_category,lambda{|quiz_category_id|where(["quiz_category_id IN (?)",quiz_category_id])}
    scope  :by_id,lambda{|question_id|where(["id IN (?)",question_id])}

	def answer_options
		self.quiz_question_options
	end

	def question_url
	    HOST_NAME + self.question_image.url
	end

	def answer_url
		HOST_NAME + self.answer_image.url
	end

	private

  ## Custom Validation Method ##
	def question_or_question_image
		if question.blank? && question_image.blank?
			self.errors.add :question, "can blank only if image for question is present (either set text for question or image)"
		end
	end

	def correct_answer_or_answer_image
		if correct_answer.blank? && answer_image.blank?
			self.errors.add :correct_answer,"can blank only if image for answewr is present (either set text for answer or image)"
		end 
	end
end
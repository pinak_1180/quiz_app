json.array!(@users) do |user|
  json.extract! user, :firstname, :lastname, :address, :city, :state, :country, :phonenumber
  json.url user_url(user, format: :json)
end
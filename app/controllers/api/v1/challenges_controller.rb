class Api::V1::ChallengesController < Api::V1::ApplicationController
  before_filter :authentication_user_with_authentication_token
  before_filter :check_params, only: :create

  def create
    @challenge = @current_user.sent_challenges.build(challenge_params)
    if @challenge.save
      @emails.zip(@names).each do |email,name|
        user = User.find_by(email: email)
        challenge_details = @challenge.challenge_details.build(reciver_email: email,reciver_name: name, reciver_id: user.present? ? user.id : nil, status: "pending")
        challenge_details.save
      end
      @challenge.challenge_extra_details
    else
      render_json({:errors => @challenge.display_errors, :status => 404}.to_json)
    end
  end
 
  def sent_challenges_list
    ##@sent_challenges_list = @current_user.sent_challenges
    @sent_challenges_list = @current_user.challenge_details.includes(:challenge)
  end

  def recived_challenges_list
    @recived_challenges_list = @current_user.recived_challenges
  end

  def show
    if params[:challenge_id].present?
      @challenge = Challenge.find(params[:challenge_id])
      render '/api/v1/challenges/create'
    else
       render_json({:errors => "Id is required", :status => 404}.to_json)
    end
  end

  def accept
    if params[:challenge_id].present?
      @challenge_details = ChallengeDetail.where("challenge_id = ? and reciver_id = ?",params[:challenge_id],@current_user.id).last
      unless @challenge_details.present?
        render_json({:errors => "No Challenge found with id #{params[:challenge_id]}", :status => 404}.to_json) 
      else
        if @challenge_details.status?("accepted")
          render_json({:errors => "Challenge is already accepted", :status => 404}.to_json) 
        else
          @challenge_details.update_attributes(status: "accepted")
          render_json({:message => "Challenge is accepted", :status => 200}.to_json)
        end
      end
    else
       render_json({:errors => "Id is required", :status => 404}.to_json)
    end
  end

  def won_lost
    if params[:challenge_id].present? && params[:time_taken].present? && params[:score].present?
      @challenge = Challenge.find(params[:challenge_id])
      @challenge_details = ChallengeDetail.where("challenge_id =? and reciver_id = ?",params[:challenge_id],@current_user.id)
      @challenge_details.last.update_attributes(score: params[:score],time_take: params[:time_taken]) if @challenge_details.present?
      if @challenge.score > params[:score].to_i && @challenge.time_take.to_i <= params[:time_taken].to_i
        @challenge.update_attributes(:challenge_won_id => @challenge.user_id)
        render '/api/v1/challenges/create'
      elsif @challenge.score < params[:score].to_i
        @challenge.update_attributes(:challenge_won_id => @current_user.id)
        render '/api/v1/challenges/create'
      elsif @challenge.score == params[:score].to_i && @challenge.time_take.to_i < params[:time_taken].to_i
        @challenge.update_attributes(:challenge_won_id => @challenge.user_id)
        render '/api/v1/challenges/create'
      elsif @challenge.score == params[:score].to_i && @challenge.time_take.to_i == params[:time_taken].to_i
        @challenge.update_attributes(:challenge_won_id => @challenge.user_id)
        render '/api/v1/challenges/create'
      end
    else
      render_json({:errors => "Parameter missing", :status => 404}.to_json)
    end 
  end

  def won_lost_statstics
    recivers_ids = User.joins(sent_challenges: :challenge_details).where("users.id = ? and challenge_details.reciver_id is not null and challenge_details.status = ?",@current_user.id,"accepted").pluck("challenge_details.reciver_id")
    senders_ids = Challenge.joins(:challenge_details).where("challenge_details.reciver_id = ? and challenge_details.status = ?",@current_user.id,"accepted").pluck("challenges.user_id")
    users = recivers_ids + senders_ids
    @users = User.where(id: users.uniq)
  end
 
private
# Never trust parameters from the scary internet, only allow the white list through.
  def challenge_params
    params.require(:challenge).permit(:session_type,:score,:no_of_question,:time_take,:category_selected)
  end

  def check_params
    if params[:reciver_email].present?
      @emails = params[:reciver_email].split(",")
      @names = params[:reciver_name].present? ? params[:reciver_name].split(",") : @emails
    else 
      render_json({:errors => "Parameter missing", :status => 404}.to_json)
    end
  end
end
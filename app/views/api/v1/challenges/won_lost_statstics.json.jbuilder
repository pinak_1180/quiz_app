json.data do
	if @users.empty?
		json.message "No Challnges to view!"
	else
		json.recivers @users.each do |user|
			json.id user.id
			json.name user.firstname
		    json.won_challenges Challenge.won_challenge(@current_user.id,user.id,user.id)
			json.lost_challenges Challenge.all_challenges(@current_user.id,user.id).length - Challenge.won_challenge(@current_user.id,user.id,user.id)
		end
	end
end

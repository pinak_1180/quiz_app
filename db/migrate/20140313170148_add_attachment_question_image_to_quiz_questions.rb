class AddAttachmentQuestionImageToQuizQuestions < ActiveRecord::Migration
  def self.up
    change_table :quiz_questions do |t|
      t.attachment :question_image
    end
  end

  def self.down
    drop_attached_file :quiz_questions, :question_image
  end
end

class AddFieldToChallengeId < ActiveRecord::Migration
  def change
    add_column     :challenges, :challenge_won_id, :integer
  end
end

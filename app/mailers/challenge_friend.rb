class ChallengeFriend < ActionMailer::Base
  default from: "no-reply@iqtrainer.com"
  default content_type: "text/html"

  def mail_for_challenge(email,sender)
  	@sender = sender
  	@email = email
    mail(:to => email, :subject => "IQ-trainer Challenge Friend")
  end
end

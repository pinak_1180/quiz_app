class QuizQuestionOption < ActiveRecord::Base

	##Paperclip##
	has_attached_file :answer,:url => "/system/answer_options/:attachment/:id_partition/:style/:filename",:styles => {:thumb => "50x50>",:large => "200x200>" ,:small => "100x100>"}

	##Associations##
	belongs_to :quiz_question

	##Validations##
	#validates :name_or_answer,:presence => true
	validates_attachment_content_type :answer, :content_type => /\Aimage\/.*\Z/

	def option_url
    HOST_NAME + self.answer.url
  end

	private

  ## Custom Validation Method ##
	def name_or_answer
		if name.blank? && answer.blank?
			self.errors.add :name, "can blank only if image for answer is present (either set text for answer or image)"
		end
	end
	
end

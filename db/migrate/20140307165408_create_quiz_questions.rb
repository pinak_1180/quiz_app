class CreateQuizQuestions < ActiveRecord::Migration
  def change
    create_table :quiz_questions do |t|
      t.integer :quiz_category_id
      t.string :question

      t.timestamps
    end
  end
end

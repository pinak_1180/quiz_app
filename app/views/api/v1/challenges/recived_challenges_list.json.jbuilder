json.data do
	if @recived_challenges_list.empty?
		json.message "No Categories to list!"
	else		
		json.array! @recived_challenges_list do |c|
			json.id  c.id
			json.sender_id c.user_id
			json.sender_email  c.sender.email
			json.sender_name  c.sender.firstname
			json.session_type     c.session_type
			json.score   c.score
			json.time_taken   c.time_take
			json.no_of_question c.no_of_question
			json.category_selected c.category_selected
			json.status c.challenge_details.by_reciver(@current_user.id).last.status
			
			json.created_at c.created_at
		end		
	end
end
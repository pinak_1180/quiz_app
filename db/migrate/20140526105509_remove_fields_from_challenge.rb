class RemoveFieldsFromChallenge < ActiveRecord::Migration
  	def self.up
  		challenges = Challenge.all
  		if challenges.present?
  			puts "in if"
		  	challenges.each do |challenge|
		  		puts "in loop"
		  		reciver_emails = challenge.reciver_email.split(",") if challenge.reciver_email.present?
		  		reciver_names = challenge.reciver_name.split(",") if challenge.reciver_name.present?
		  		if reciver_emails.present? && reciver_names.present?
			  		reciver_emails.zip(reciver_names).each do |email,name|
			  			user = User.find_by(email: email)
			  			ChallengeDetail.create(challenge_id: challenge.id,reciver_email: email,reciver_name: name, reciver_id: user.present? ? user.id : nil, status: "pending")
			  		end
			  	end
  			end
  		end
    	remove_column :challenges, :reciver_name
    	remove_column :challenges, :reciver_email
    	remove_column :challenges, :reciver_id
	end

	def self.down
  		add_column :challenges, :reciver_name, :string
    	add_column :challenges, :reciver_email, :string
    	challenges = Challenge.all
  		if challenges.present?
		    challenges.each do |challenge|
		    	challenge.reciver_email = ""
		    	challenge.reciver_name = ""
		    	challenge_details = challenge.challenge_details
		    	if challenge_details.present?
			    	challenge_details.each do |challenge_detail|
			    		challenge.reciver_email = challenge.reciver_email + challenge_detail.reciver_email + ","
			    		challenge.reciver_name = challenge.reciver_name + challenge_detail.reciver_name + ","
			    	end
			    end
		    	challenge.save
	    	end
	    end
	end
end

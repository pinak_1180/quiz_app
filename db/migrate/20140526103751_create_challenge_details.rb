class CreateChallengeDetails < ActiveRecord::Migration
  def change
    create_table :challenge_details do |t|
      t.string :reciver_email
      t.string :reciver_name
      t.integer :reciver_id
      t.string :status
      t.float :score
      t.string :time_take
      t.integer :challenge_id

      t.timestamps
    end
  end
end

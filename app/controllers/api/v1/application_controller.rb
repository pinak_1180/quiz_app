class Api::V1::ApplicationController < ActionController::Base
	rescue_from ActionController::RoutingError,:with => :bad_record
  rescue_from ActiveRecord::RecordNotFound, :with => :bad_record

	protected

	def authentication_user_with_authentication_token
    @current_user = AuthenticationToken.find_user_from_authentication_token(params[:authentication_token])    
    unless @current_user.present?
      render_json({:errors => "You required to register or login before continue to this action!", :status => 401}.to_json)
    end
  end

  def render_json(json)
    callback = params[:callback]
    response = begin
      if callback
        "#{callback}(#{json});"
      else
        json
      end
    end
    render({:content_type => :js, :text => response})
  end

  def bad_record
    render_json({:errors => "No Record Found!", :status => 404}.to_json)
  end

end
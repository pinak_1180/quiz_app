class Api::V1::RegistrationsController < Api::V1::ApplicationController
 def create
    @user = User.new(user_params)
    @user.check_duplicate_device_ids(params[:user][:device_id],@user,params[:user][:device_type])
    unless @user.save
      render_json({:errors => @user.errors.full_messages.join(","), :status => 404}.to_json)
    else
     @authentication_token = @user.authentication_tokens.create(:auth_token => AuthenticationToken.generate_unique_token)
     @user.save
    end
end
private
# Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email,:password,:password_confirmation,:firstname, :lastname, :address, :city, :state, :country, :phonenumber,:device_id,:device_type)
    end
end

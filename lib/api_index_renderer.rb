class ApiIndexRenderer
  attr_reader :name, :link_ref, :method_type
  
  def initialize(name, link_ref, method_type)
    @name        = name
    @link_ref    = link_ref
    @method_type = method_type
  end
  
  class << self
    def passenger_api_index
      index_arr = []
      index_arr << ApiIndexRenderer.new("Signup", "signup", "POST")
      index_arr << ApiIndexRenderer.new("Login", "login", "POST")
      index_arr << ApiIndexRenderer.new("Forgot Password","forgot_password","POST")
      index_arr << ApiIndexRenderer.new("Change Password","change_password","POST")
      index_arr << ApiIndexRenderer.new("Logout", "logout", "GET")
      index_arr << ApiIndexRenderer.new("List Categories","list_categories","GET")
      index_arr << ApiIndexRenderer.new("Fetch Question Category wise Free","fetch_question","GET")
      index_arr << ApiIndexRenderer.new("Fetch Question Category wise Pro","fetch_question_pro","GET")
      index_arr << ApiIndexRenderer.new("Fetch Question By Entering Category ID","question_by_category","GET")
      index_arr << ApiIndexRenderer.new("Fetch Question By Entering Multiple Category ID","questions_by_multiple_category","GET")
      index_arr << ApiIndexRenderer.new("Quick Session","question_for_quick_session","GET")
      index_arr << ApiIndexRenderer.new("Global Avarage","global_avarage_for_questions","POST")
      index_arr << ApiIndexRenderer.new("Create Challenge","create_challenge","POST")
      index_arr << ApiIndexRenderer.new("Sent challenges list","sent_challenge","GET")
      index_arr << ApiIndexRenderer.new("recived challenges list","recived_challenge","GET")
      index_arr << ApiIndexRenderer.new("get challenge details","show_challenge","GET")
      index_arr << ApiIndexRenderer.new("Accept Challenge","accept_challenge","POST")
      index_arr << ApiIndexRenderer.new("Won/lost","won_lost","POST")
      index_arr << ApiIndexRenderer.new("Challenge statistics","challenge_stat","GET")
      index_arr
    end    
  end
end
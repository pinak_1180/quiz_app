json.data do
	if @categories.empty?
		json.message "No Categories to list!"
	else		
		json.array! @categories do |category|
			json.id  category.id
			json.name  category.name
		end		
	end
end